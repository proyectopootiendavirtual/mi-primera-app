package facci.gustavoreyes.miprimeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    Button btnconvertir =null;
    TextView resulfinal =null;
    Spinner spincl=null;
    EditText cantidad = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnconvertir =(Button) findViewById(R.id.buttonConvert);
        cantidad=(EditText) findViewById(R.id.numeroingresado);
        resulfinal =(TextView) findViewById(R.id.result);
        spincl=(Spinner) findViewById(R.id.spineerop);


        String[]op={"Selecciona una opcion", "Centígrados a Fahrenheit ","Fahrenheit  a Centígrados"};

        ArrayAdapter<String> adapter=new
                ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,op);

        spincl.setAdapter(adapter);
        btnconvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cantidad.getText().toString().equals("")){
                    Toast msg=Toast.makeText(getApplicationContext(),"Ingresa un dato",Toast.LENGTH_SHORT);
                    msg.show();
                }else{
                    Double c=Double.parseDouble(cantidad.getText().toString());
                    Double res=null;
                    int select=spincl.getSelectedItemPosition();

                    switch (select){
                        case 0:
                            res=0.0;
                            Toast.makeText(getApplicationContext(),"Selecciona la conversión",Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            res=1.8*c+32;
                            break;
                        case 2:
                            res=(c-32)/1.8;
                            break;

                    }
                    resulfinal.setText(res.toString());
                }
            }
        });
    }
}